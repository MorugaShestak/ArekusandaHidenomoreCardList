import './App.css';
import Card from "./components/Card/Card";
import {useEffect, useState} from "react";

function App() {

    const [overallCount, setOverallCount] = useState(0)
    const [domainCount, setDomainCount] = useState(0)
    const [creatureCount, setCreatureCount] = useState(0)
    const [artifactCount, setArtifactCount] = useState(0)
    const [spellCount, setSpellCount] = useState(0)
    const [resourceCount, setResourceCount] = useState(0)
    const [buildingCount, setBuildingCount] = useState(0)

    const [commonCount, setCommonCount] = useState()
    const [uncommonCount, setUncommonCount] = useState()
    const [rareCount, setRareCount] = useState()
    const [epicCount, setEpicCount] = useState()
    const [mythCount, setmythCount] = useState()

    useEffect(() => {
        setDomainCount(document.querySelectorAll('#domain').length)
        setCreatureCount(document.querySelectorAll('#creature').length)
        setArtifactCount(document.querySelectorAll('#artifact').length)
        setSpellCount(document.querySelectorAll('#spell').length)
        setBuildingCount(document.querySelectorAll('#building').length)
        setResourceCount(document.querySelectorAll('#resource').length)
        setCommonCount(document.getElementsByClassName("common").length)
        setUncommonCount(document.getElementsByClassName("uncommon").length)
        setRareCount(document.getElementsByClassName("rare").length)
        setEpicCount(document.getElementsByClassName("epic").length)
        setmythCount(document.getElementsByClassName("myth").length)
        setOverallCount(commonCount + uncommonCount + rareCount + epicCount + mythCount)
    }, [])


    return (
        <div className="App">

            <details>
                <summary>Описание способности</summary>
                <span className={"abilityDesc"}>
            "Игра – это жизнь" <br/>

Техника Арекусанды представляет собой Настольную Коллекционную Карточную Игру, которую тот придумал ещё в далёком детстве. <br/>

На момент начала игры у Арекусанды имеется 80 карт в колоде. <br/>
Каждая карта способна перемещаться на расстояние до 100 метров от Шамана. <br/>
Скорость карт = скорости Шамана, а активируются карты мгновенно, если не написано обратное. <br/>

Суть данной ККИ в уничтожении противника не только с помощью призыва существ и использования разных заклинаний, но также победа с использованием разных “Территорий”, которые вносят свои корректировки в игру.
<br/>
В ККИ содержаться следующие типы карт:<br/>
- Существа – Данный тип карт призывает различных Существ-Шикигами. Все карты Существ имеют “Боевой Клич”, используя который, Существо моментально пропадает, активируя тем последствия Боевого Клича.<br/>
- Артефакты – Данный тип карт призывает различные артефакты.<br/>
- Территории – Данный тип карт заключает определённую область сражения в “Завесу” со специальными условиями для игрока.<br/>
- Заклинание – Данный тип карт призывает различные заклинания.<br/>
- Постройка – Данный тип карт призывает строение, выполняющее ту или иную функцию.<br/>
- Ресурс - Данный тип карт представляет собой разнообразные ресурсы, которые нельзя получить просто из колоды. <br/>

<hr className={"divider line glow"}/><br/>
            Каждая карта содержит следующее содержание:
<br/>
1.	Наименование карты<br/>
2.	Тип карты<br/>
3.	Стоимость в CP или P+<br/>
4.	Описание<br/>
5.	Принадлежность карты<br/>
<br/>
В случае, если карта относиться к типу Существо/Артефакт/Заклинание/Постройка, карта также содержит информацию о характеристиках призываемого энкаутера.
<br/>
Принадлежность карты, то есть 5 пункт, полностью связан с механикой “Территорий”, которые вносят свои корректировки в способность Арекусанды.
<br/>
<hr className={"divider line glow"}/><br/>
<br/>
При использовании карты “Территория”, автоматически создаётся завеса радиусом, до 2-х километров, а внутри самой завесы внешний вид территории начинает меняться на выбранную территорию, однако он не просто стирается, заменяясь на новый. Территория смешивается с текущим окружением Шамана.
Например, если призвать Территорию Леса в центре города, то деревья начнут появляется из-под асфальта и дорог.
Эффект исчезает при уничтожении Завесы.
<br/>
Каждая Территория имеет какое-то определённое свойство, базирующееся на 9 основных Принадлежностях:
<br/>
- <span style={{color: "#3b6e31"}}><b>Дендро</b></span> (Природа)<br/>
- <span style={{color: "#bf2818"}}><b>Пиро</b></span> (Жар, огонь, лава и всё связанное)<br/>
- <span style={{color: "#0b4dda"}}><b>Гидро</b></span> (Вода, влажность и всё связанное)<br/>
- <span style={{color: "#2effea"}}><b>Крио</b></span> (Холод, снег, лёд и всё связанное)<br/>
- <span style={{color: "#9336b0"}}><b>Электро</b></span> (Электричесво, молнии, бури)<br/>
- <span style={{color: "#26a684"}}><b>Анемо</b></span> (Ветер, воздух, свобода)<br/>
- <span style={{color: "#B68D07"}}><b>Гео</b></span> (горы, равнины, скалистые местности и всё связанное)<br/>
- <span style={{color: "#d2d461"}}><b>Хроно</b></span> (территории, связанные с памятниками истории или религий)<br/>
- <span style={{color: "#5c5c5c"}}><b>Бездна</b></span> (территории, связанные с марком, неизвестностью и пустотой)
<br/>
У одной Территории может быть несколько принадлежностей.
<br/>
Если при разыгрывании карты Существа/Артефакта/Заклинание оно имеет ту же принадлежность, что и Территория, на которой разыгрывают карту, то карта получает пассивный бафф к РП и Прочности в 1.25x раз.
<br/>
Существа и принадлежности взаимоисключающие:
                <br/>
                На <span style={{color: "#bf2818"}}><b>Пиро</b></span> территориях нельзя использовать <span
                    style={{color: "#0b4dda"}}><b>Гидро</b></span>, <span
                    style={{color: "#2effea"}}><b>Крио</b></span> или <span
                    style={{color: "#3b6e31"}}><b>Дендро</b></span> Карты и наоборот.<br/>
                На <span style={{color: "#B68D07"}}><b>Гео</b></span> территориях нельзя использовать <span
                    style={{color: "#26a684"}}><b>Анемо</b></span> и <span
                    style={{color: "#9336b0"}}><b>Электро</b></span> Карты и наоборот.
                <br/>
<hr className={"divider line glow"}/><br/>
<br/>
Каждая из карт имеет редкость, от <span style={{color: "#7d7d7d"}}><b>Обычной</b></span>, до <span
                    style={{color: "#00BA78FF"}}><b>Мифической</b></span>, в порядке.
<br/>
Редкость карты определяет её цену и силу.
<br/> <br/>
- <span style={{color: "#7d7d7d"}}><b>Обычная</b></span> - Невзрачные карты минимально пригождающиеся в бою. За ненадобностью были полностью удалены из Общего Пула Карт.<br/>
- <span style={{color: "#ded8d3"}}><b>Необычная</b></span> - Представляют собой карты, которыми обычно характеризуют ответвления от Техник, т.е. простые техники и взаимодействия.<br/>
- <span style={{color: "#f5ff30"}}><b>Редкая</b></span> - Представляют собой карты с уникальными возможностями, похожими на полноценные подтехники. Часто могут быть использованы как основное оружие в бою. <br/>
<span style={{color: "#ac30ff"}}><b>Эпическая</b></span> - Представляют собой карты, которые в определённой комбинации могут потянуть на полноценную технику и заставить попотеть противника равного ранга.<br/>
- <span style={{color: "#00BA78FF"}}><b>Мифическая</b></span> - Карты, имеющие огромнейший потенциал, каждая из которых может равняться полноценной технике. Имеют огромную стоимость и риски, но взамен дают значительное преимущество.<br/>
<br/>
<hr className={"divider line glow"}/><br/>
Для использования своей Техники, Арекусанде нужно сначала призвать шикигами в виде книги, который содержит в себе все карты.
<br/>
У шикигами нет ограничения по страницам, и Шаман способен получать новые карты раз за разом.
<br/>
                Шикигами способен летать и перелистывать страницы со скоростью, равной скорости Шамана.
                <br/>
Стоимость призыва – 5 000 CP
Прочность – 10 000 CP
<br/>
<hr className={"divider line glow"}/><br/>
                <h3>Для полноценной битвы с кем-либо Арекусанде нужно составить "колоду" против противника</h3>
                После призыва Шикигами время для Арекусанды полностью останавливается и у него есть неограниченное кол-во этого самого времени, чтобы составить колоду, которую он будет использовать.<br/>
                Допустимое количество карт в колоде высчитывается в зависимости от ранга противника: <br/>
                Если ранги Шамана и Оппонента равны, то в колоде можно использовать 20 карт.<br/>
                За каждый ранг в разнице, в пользу Шамана из колоды вычитаются 5 карт, а также по одной карте редкости из колоды.<br/>
                За каждый ранг в разнице, в пользу Оппонента к колоде добавляются 5 карт, а также по одной карте редкости из колоды.<br/><br/>

                Колода составляется для каждого противника и в случае, если противников несколько, то для каждого противника составляется своя колода карт, однако использовать карты можно как угодно.<br/>
<hr className={"divider line glow"}/><br/>

        </span>
            </details>

            <h2>Всего карт - {commonCount + uncommonCount + rareCount + epicCount + mythCount}</h2>
            <h3><span style={{color: "#7d7d7d"}}><b>Обычных</b></span> карт - {commonCount}</h3>
            <h3><span style={{color: "#ded8d3"}}><b>Необычных</b></span> карт - {uncommonCount}</h3>
            <h3><span style={{color: "#ffbf80"}}><b>Редких</b></span> карт - {rareCount}</h3>
            <h3><span style={{color: "#ac30ff"}}><b>Эпических</b></span> карт - {epicCount}</h3>
            <h3><span style={{color: "#00BA78FF"}}><b>Мифических</b></span> карт - {mythCount}</h3>

            <details>
                <summary>Карты Территорий | всего - {domainCount}</summary>
                <br/>
                <br/>

                {/* placeholder */}
                <Card name={"Мастерская Гефеса"} cost={"10 000 CP"} type={'domain'} rare={'myth'}
                      description={"Мастерская Гефеса создаёт внутри маленькую кузницу со статуей Гефеса, который периодически бьёт молоток по наковальне\n" +
                          "Прочность кузницы - 15 000 CP\n" +
                          "В этой Территории любой Артефакт, который призвал пользователь, увеличивает свои Характеристики в 1.5х раза"}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Тихий Омут"} cost={"10 000 CP"} type={'domain'} rare={'myth'}
                      description={"Тихий Омут превращает любые поверхности, которые можно идентифицировать как пол, в той или иной системе координат\n" +
                          "Помимо прочего, в Тихом Омуте всё перестаёт издавать звуки. Всё становиться абсолютно бесшумным\n" +
                          "Умибозу получает усиление Тела на 2 000 CP внутри Территории\n" +
                          "Умибозу способен уходить под воду и выныривать из неё в Тихом Омуте"}/>
                {/* placeholder */}


            </details>

            <details>
                <summary>Карты Существ | всего - {creatureCount}</summary>
                <br/>
                <br/>

                {/* placeholder */}
                <Card name={"Умибозу"} cost={"15 000 CP"} rare={"epic"} type={"creature"}
                      description={"Умибозу - это морское чудовище с круглым ртом, усеянным острыми зубами, вокруг которого растут щупальца. Правая из его рук похожа на раскрывающуюся ракушку, а левая - на острый коралл, заточенный под меч. Весь он худощав и грозен в своей ужасающей простоте.\n" +
                          "\nХарактеристики:\n" +
                          "Сила - 2 000 CP\n" +
                          "Сила Клинка на левой руке - 5 000 CP" +
                          "Прочность - 2 000 CP\n" +
                          "Прочность Ракушки на правой руке - 5 000 CP\n"
                          + "Принадлежность: Гидро"}/>
                {/* placeholder */}

                {/* placeholder */}
                 <Card name={"Безумный Провидец"} cost={"10 000 CP"} rare={"rare"} type={"creature"}
                      description={"Призывает четырёхрукое левитирующее существо, которое сразу после призыва начинает использовать свою технику и создавать большой тёмный шар над своей головой\n" +
                          "\nХарактеристики:\n" +
                          "Сила - 0 CP\n" +
                          "Прочность - 5 000 CP\n" +
                          "Безумному Провидцу нужен 1 полный пост Существования, чтобы узнать всю информацию о каком либо из Существ.\nВся эта информация передаётся Арекусанде при смерти или отмене призыва.\n"
                          + "Принадлежность: Бездна"}/>
                
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Владыка Бури"} cost={"15 000 CP"} rare={"epic"} type={"creature"}
                      description={"Призывает большую 4-х крылую Сову, именующуюся Владыка Бури.\n" +
                          "\nХарактеристики:\n" +
                          "Сила - 5 000 CP\n" +
                          "Прочность - 5 000 CP\n\n" +
                          "Способность:\n" +
                          "Острие Ветра - За 2 000 CP создаёт острый воздушный разрез с РП и Прочность = 3 000, а скорость = Шаман" +
                          "Яростный Поток - Взмахом Крыла создаёт воздушный поток со скоростью = Шаман, радиусом в 0.5 метров и РП и Прочность = 4 000 CP. Стоит 2 500 CP\n" +
                          "Цепная Молния - Призыв молнии за 2.000 CP, с РП и Прочностью = 2.000 CP, и скоростью = Шаман. При попадании по цели, даже если та защитилась ПЭ, наносит дополнительные 1.000 CP урона всем целям, находящимся в радиусе 10-х метров от поражённой цели.\n"
                          + "Принадлежность: Анемо и Электро"}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Чэйнсоу"} cost={"20 000 CP"} rare={"epic"} type={"creature"}
                      description={"Чэйнсоу - это монстр, усеянный бензопилами. Его голова выглядит как бензопила, а из рук, ног и всего тела торчит множество бензопил.\n" +
                          "\nХарактеристики:\n" +
                          "Сила - 4 000 CP\n" +
                          "Прочность - 4 000 CP\n\n" +
                          "Чэйнсоу способен отцеплять цепь у бензопи. Их длинна - 30 метров\n"
                          + "Принадлежность: Бездна"}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Сова"} cost={"25 000 CP"} rare={"myth"} type={"creature"}
                      description={"Сова - Выглядит как Какуджа Совы из Токийского Гуля\n" +
                          "\nХарактеристики:\n" +
                          "Сила - 5 000 CP\n" +
                          "Прочность - 5 000 CP\n\n" +
                          "Сова способна регенерировать за счёт своей Проклятой Энергии или ПЭ Шамана по расценке Проклятых Духов\n"
                          + "Принадлежность: Бездна"}/>
                {/* placeholder */}

                {/* placeholder */}
                 <Card name={"Леший"} cost={"20 000 CP"} rare={"rare"} type={"creature"}
                      description={"Призывало большое чудовище, именуемо Лешим, олицетворяющим весь ужас и гнев природы\n" +
                          "\nХарактеристики:\n" +
                          "Сила - 5 000 CP\n" +
                          "Прочность - 5 000 CP\n" +"Предел: 25 000 CP\n\n"
                          + "Принадлежность: Бездна и Дендро"}/>
                {/* placeholder */}

            </details>


            <details>
                <summary>Карты Артефактов | всего - {artifactCount}</summary>
                <br/>
                <br/>
                {/* placeholder */}
                <Card name={"Лос Лобос"} cost={"5 000 CP"} rare={"rare"} type={"artifact"}
                      description={"Лос Лобос представляет собой пистолет, способный накапливать на конце ствола синюю Энергию, называемую Серо, которой он способен затем выстрелить. \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 1 000 CP\n" +
                          "В снаряд можно вложить до 5 000 CP с модификатором 1.5х\n\n"
                          + "Принадлежность: Бездна "}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Частифол"} cost={"15 000 CP"} rare={"epic"} type={"artifact"}
                      description={"Призывает золотое копьё 'Частифол'. Копьё управляется силой мысли, а вливать в него ПЭ можно дистанционно\n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 3 000 CP\n\n"
                          + "Принадлежность: Хроно и Дендро "}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Тессен"} cost={"20 000 CP"} rare={"myth"} type={"artifact"}
                      description={"Артефакт представляет собой два железных веера. \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 4 000 CP\n" +
                          "Способность:\n" +
                          "Криокинез - созидание Льда. 1 м³ льда стоит от 1 000 CP до 5 000 CP и имеет Прочность = {Вложенное СP + 1 000 CP}\n\n"
                          + "Принадлежность: Хроно и Дендро "}/>
                {/* placeholder */}
            </details> 

            <details>
                <summary>Карты Заклинаний | всего - {spellCount}</summary>
                <br/>
                <br/>
                {/* placeholder */}
                <Card name={"Пайро Пуля"} cost={"2 500CP"} rare={"uncommon"} type={"spell"}
                      description={"Создаёт маленькую огненую пулю размерами 1 х 1.5 сантиметра, который после призыва на большой скорости летит вперёд.                      \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 3 500 CP\n\n"
                          + "Принадлежность: Пайро "}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Гидро Пуля"} cost={"2 500CP"} rare={"uncommon"} type={"spell"}
                      description={"Создаёт маленькую водяную пулю размерами 1 х 1.5 сантиметра, который после призыва на большой скорости летит вперёд.                      \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 3 500 CP\n\n"
                          + "Принадлежность: Гидро "}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Электро Пуля"} cost={"2 500CP"} rare={"uncommon"} type={"spell"}
                      description={"Создаёт маленькую электрическую пулю размерами 1 х 1.5 сантиметра, который после призыва на большой скорости летит вперёд.                      \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 3 500 CP\n\n"
                          + "Принадлежность: Электро "}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Гео Пуля"} cost={"2 500CP"} rare={"uncommon"} type={"spell"}
                      description={"Создаёт маленькую каменную пулю размерами 1 х 1.5 сантиметра, который после призыва на большой скорости летит вперёд.                      \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 3 500 CP\n\n"
                          + "Принадлежность: Гео "}/>
                {/* placeholder */}

                {/* placeholder */}
                <Card name={"Крио Пуля"} cost={"2 500CP"} rare={"uncommon"} type={"spell"}
                      description={"Создаёт маленькую ледяную пулю размерами 1 х 1.5 сантиметра, который после призыва на большой скорости летит вперёд.                      \n" +
                          "\nХарактеристики:\n" +
                          "Сила и Прочность - 3 500 CP\n\n"
                          + "Принадлежность: Крио "}/>
                {/* placeholder */}
            </details>

            <details>
                <summary>Карты Построек | всего - {buildingCount}</summary>
                <br/>
                <br/>

            </details>

            <details>
                <summary>Карты Ресурсов | всего - {resourceCount}</summary>
                <br/>
                <br/>

            </details>

        </div>
    );
}

export default App;
